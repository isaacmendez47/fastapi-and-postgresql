steps = [
    [
        ## Create the table
				# every line in the parenthesis seems to add a colum
				# since we're making our own data base, we need to it be
				# be serialized. we do this with the first time
				# we can add any data types found in
				# postgresql.org/docs/current.datatype.html
				# dont have to capitalize anything but it helps. be consistent
				"""
        CREATE TABLE vacations (
					id SERIAL PRIMARY KEY NOT NULL,
          name VARCHAR(1000) NOT NULL,
          from_date DATE NOT NULL,
          to_date DATE NOT NULL,
          thoughts TEXT
				);
        """, # the comma is added because its a list
				## Drop the table
				"""
        DROP TABLE vacations;
        """
		]
]
