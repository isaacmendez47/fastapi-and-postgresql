from pydantic import BaseModel
from typing import Optional, List, Union
from datetime import date
from queries.pool import pool

# this has nothing to do with the database, this is for
# our endpoints!

class Error(BaseModel):
  message: str

class VacationIn(BaseModel):
  name: str
  from_date: date
  to_date: date
  thoughts: Optional[str]

class VacationOut(BaseModel):
  id: int
  name: str
  from_date: date
  to_date: date
  thoughts: Optional[str]

class VacationRepository:

  def get_one(self, vacation_id:int) -> Optional[VacationOut]:
    try:
      # connect to database
      with pool.connection() as conn:
        # get cursour (something to run sql with)
        with conn.cursor() as db:
          result = db.execute(
            """
            SELECT id
            , name
            , from_date
            , to_date
            , thoughts
            FROM vacations
            WHERE id = %s
            """,
            [vacation_id]
          )
          record = result.fetchone()
          if record is None:
            return None
          return self.record_to_vacation_out(record)
    except Exception as e:
      print(e)
      return {"message" : "Could not get vacation"}


  def delete(self, vacation_id:int) -> bool:
    try:
      # connect to database
      with pool.connection() as conn:
        # get cursour (something to run sql with)
        with conn.cursor() as db:
          db.execute(
            """
            DELETE FROM vacations
            WHERE id = %s
            """,
            [vacation_id]
          )
          return True
    except Exception as e:
      print(e)
      return False


  def update(self, vacation_id: int, vacation: VacationIn) -> Union[VacationOut, Error]:
    try:
      # connect to database
      with pool.connection() as conn:
        # get cursour (something to run sql with)
        with conn.cursor() as db:
          db.execute(
            """
            UPDATE vacations
            SET name = %s
              , from_date = %s
              , to_date = %s
              , thoughts = %s
            WHERE id = %s
            """,
            [
              vacation.name,
              vacation.from_date,
              vacation.to_date,
              vacation.thoughts,
              vacation_id
            ]
          )
          return self.vacation_in_to_out(vacation_id, vacation)
          # instead of using these two lines, we can create a separate
          # function to do the same thing, and in the future, just change that function
          # since its going to be used in more functions that these
          # old_data = vacation.dict()
          # return VacationOut(id=vacation_id, **old_data)


    except Exception as e:
      print(e)
      return {"message" : " could not update vacation"}



  def get_all(self) -> Union[Error, List[VacationOut]]:
    try:
      # connect to database
      with pool.connection() as conn:
        # get cursour (something to run sql with)
        with conn.cursor() as db:
          # run our insert statement
          result = db.execute(
            """
            SELECT id, name, from_date, to_date, thoughts
            FROM vacations
            ORDER BY from_date;
            """
          )
          result = []
          for record in db:
            vacation = VacationOut(
                id=record[0],
                name=record[1],
                from_date =record[2],
                to_date = record[3],
                thoughts = record[4]
            )
            result.append(vacation)
          return result
    except Exception as e:
      print(e)
      return {"message" : " could not get all vacation"}


  def create(self, vacation: VacationIn) -> VacationOut:
    # connect to database
    with pool.connection() as conn:
      # get cursour (something to run sql with)
      with conn.cursor() as db:
        # run our insert statement
        result = db.execute(
              """
              INSERT INTO vacations
                (name, from_date, to_date, thoughts)
              VALUES
								(%s, %s, %s, %s)
              RETURNING id;
              """,
              [
                    vacation.name,
                    vacation.from_date,
                    vacation.to_date,
                    vacation.thoughts
              ]
				)

        id = result.fetchone()[0]
        #return new data


        return self.vacation_in_to_out(id, vacation)
        # old_data = vacation.dict()
        #   # return {"message" : "error!"}
        # return VacationOut(id=id, **old_data)


  def vacation_in_to_out(self, id:int, vacation:VacationIn):
        old_data = vacation.dict()
        return VacationOut(id=id, **old_data)

  def record_to_vacation_out(self, record):
    return VacationOut(
      id=record[0],
      name=record[1],
      from_date=record[2],
      to_date=record[3],
      thoughts=record[4],
    )
